const express = require('express')
const app = express()
app.use(express.json());
const port = 3000


function nuevoJuego() {
    let juego = []
    for (let i = 0; i < 30; i++) {
        let fila = []
        for (let j = 0; j < 30; j++) {
            fila.push(0)
        }
        juego.push(fila)
    }
    juego[15][15] = 1
    return juego;
}

function generarComida(juego) {
    let done = false
    while (!done) {
        let linea = Math.floor(Math.random() * juego.length);
        let lineaIndex = Math.floor(Math.random() * juego[0].length)
        if (juego[linea][lineaIndex] !== 0) {
            continue
        }
        juego[linea][lineaIndex] = -1
        done = true
    }
    return juego
}

function nuevaPartida(){
    return {
        juego: generarComida(nuevoJuego()),
        direccion: {
            x: 0,
            y: -1
        },
        ultimaPos: {
            x: 15,
            y: 15,
        },
        puntos: 0
    }
}


app.get('/nuevo', (req, res) => {
    return res.json(nuevaPartida())
})

app.post('/jugar', (req, res) => {
    
    let body = req.body.estadoAnterior
    let nuevaPos = {};

    switch (body.movimiento) {
        case "arriba":
            nuevaPos = {
                x: body.ultimaPos.x,
                y: body.ultimaPos.y - 1
            }
            body.direccion.x = 0;
            body.direccion.y = -1;
            break;
        case "abajo":
            nuevaPos = {
                x: body.ultimaPos.x,
                y: body.ultimaPos.y + 1
            }
            body.direccion.x = 0;
            body.direccion.y = 1;
            break;
        case "der":
            nuevaPos = {
                x: body.ultimaPos.x + 1,
                y: body.ultimaPos.y
            }
            body.direccion.x = 1;
            body.direccion.y = 0;
            break;
        case "izq":
            nuevaPos = {
                x: body.ultimaPos.x - 1,
                y: body.ultimaPos.y
            }
            body.direccion.x = -1;
            body.direccion.y = 0;
            break;
        default:
            nuevaPos = {
                x: body.ultimaPos.x + body.direccion.x,
                y: body.ultimaPos.y + body.direccion.y
            }
            break;
    }

    if (nuevaPos.x < 0 || nuevaPos.x >= body.juego[0].length) {
        body.puntos = 0;
        body.juego = nuevoJuego()
        return res.json(body)
    }
    if (nuevaPos.y < 0 || nuevaPos.y >= body.juego.length) {
        body.puntos = 0;
        body.juego = nuevoJuego()
        return res.json(body)
    }

    if (body.juego[nuevaPos.y][nuevaPos.x] < 0) {
        body.puntos += 1
        body.juego[nuevaPos.y][nuevaPos.x] = -2
        body.juego = generarComida(body.juego)
    }

    // for (let linea = 0; linea < body.juego.length; linea++) {
    //     for (let lineaKey = 0; lineaKey < body.juego[0].length; lineaKey++) {
    //         if (body.juego[linea][lineaKey] !== 0) {
    //             if (body.juego[linea][lineaKey] > body.puntos) {
    //                 body.juego[linea][lineaKey] = 0
    //             } else if (body.juego[linea][lineaKey] === -2) {
    //                 body.juego[linea][lineaKey] = 1
    //             } else if (body.juego[linea][lineaKey] !== -1) {
    //                 body.juego[linea][lineaKey] += 1
    //             }
    //         }
    //     }
    // }

    body.juego[nuevaPos.y][nuevaPos.x] = 1
    body.juego[body.ultimaPos.y][body.ultimaPos.x] = 0;
    body.ultimaPos = nuevaPos

    return res.json(body)

})


if (!module.parent) {
    app.listen(port, () => {
        console.log(`Example app listening at http://localhost:${port}`)
    })
}

module.exports = {
    app,
    generarComida,
    nuevoJuego,
    nuevaPartida
};