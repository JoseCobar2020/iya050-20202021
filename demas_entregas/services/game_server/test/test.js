var chai = require('chai')
    , chaiHttp = require('chai-http');
chai.use(chaiHttp);
const {app, nuevoJuego, generarComida, nuevaPartida} = require('../src/index')

describe("UNIT", () => {
    it("nuevaPartida", (done) => {
        let v = nuevaPartida();
        chai.expect(v.direccion).to.be.eql({
            x: 0,
            y: -1
        })
        chai.expect(v.ultimaPos).to.be.eql({
            x: 15,
            y: 15,
        })
        chai.expect(v.puntos).to.be.eql(0)
        chai.expect(v.juego).to.have.length(30)
        chai.expect(v.juego[0]).to.have.length(30)
        done()
    });

    it("nuevoJuego", (done) => {
        let v = nuevoJuego();
        chai.expect(v).to.have.length(30)
        chai.expect(v[0]).to.have.length(30)
        done()
    });

    it("generarComida", (done) => {
        let v = generarComida([[0]]);
        chai.expect(v[0][0]).to.be.eql(-1)
        done()
    });
});

describe("E2E", () => {
    it("generar", (done) => {
        chai.request(app)
            .get('/')
            .end((err, res) => {
                chai.expect(res).to.have.status(200)
                chai.expect(res).to.be.json

                let v = res.body

                chai.expect(v.direccion).to.be.eql({
                    x: 0,
                    y: -1
                })
                chai.expect(v.ultimaPos).to.be.eql({
                    x: 15,
                    y: 15,
                })
                chai.expect(v.puntos).to.be.eql(0)
                chai.expect(v.juego).to.have.length(30)
                chai.expect(v.juego[0]).to.have.length(30)
                done();
            });
    });

    it("comer", (done) => {
        let partida = nuevaPartida()
        partida.juego[14][15] = -1
        partida.direccion.y = 1
        chai.request(app)
            .post('/jugar')
            .send(partida)
            .end((err, res) => {
                chai.expect(res).to.have.status(200)
                chai.expect(res).to.be.json

                let v = res.body

                chai.expect(v.direccion).to.be.eql({
                    x: 0,
                    y: 1
                })
                chai.expect(v.ultimaPos).to.be.eql({
                    x: 15,
                    y: 14,
                })
                chai.expect(v.puntos).to.be.eql(1)
                chai.expect(v.juego).to.have.length(30)
                chai.expect(v.juego[0]).to.have.length(30)
                done();
            });
    });

    it("perder", (done) => {
        let partida = nuevaPartida()
        partida.juego = [[1]]
        partida.direccion.y = -1
        partida.direccion.y = -1
        partida.puntos = 100
        chai.request(app)
            .post('/jugar')
            .send(partida)
            .end((err, res) => {
                chai.expect(res).to.have.status(200)
                chai.expect(res).to.be.json

                let v = res.body

                chai.expect(v.direccion).to.be.eql({
                    x: 0,
                    y: -1
                })
                chai.expect(v.ultimaPos).to.be.eql({
                    x: 15,
                    y: 15,
                })
                chai.expect(v.puntos).to.be.eql(0)
                chai.expect(v.juego).to.have.length(30)
                chai.expect(v.juego[0]).to.have.length(30)
                done();
            });
    });
});
