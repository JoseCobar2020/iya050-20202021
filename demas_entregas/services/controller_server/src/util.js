const fetch = require("node-fetch");

const GQL_url = process.env.STATS_SERVER_URL || "http://localhost:4000";
const GAME_url = process.env.GAME_SERVER_URL || "http://localhost:3000";

async function getGame(gameId) {

    const query = `query{
        runInfo(
          id: ${gameId}
        ){
          id
          juego
          direccion
          ultimaPos
          puntos
        }
      }`;
    return await fetch(GQL_url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        query,
      })
    })
      .then(res => res.json())
      .then(data => { return data.data.runInfo });
};

async function saveGame(state) {

    const query = `mutation{
            setRun(
                id: ${state.id}
                player: "${state.nickname}"
                juego: "${JSON.stringify(state.juego)}"
                direccion: "${JSON.stringify(state.direccion).replace(/"/g, "'")}"
                ultimaPos: "${JSON.stringify(state.ultimaPos).replace(/"/g, "'")}"
                puntos: ${state.puntos}
            ){
                id
                juego
                direccion
                ultimaPos
                puntos
            }
            }`;
    return await fetch(GQL_url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        query,
      })
    })
      .then(res => res.json())
      .then(data => { return data.data.setRun });
}

async function createGame() {

    let response = await fetch(`${GAME_url}/nuevo`)
    .then(res => res.json())
    .then(data => data);
    return response;

}

async function handleE(estadoAnterior) {

    let response = await fetch(`${GAME_url}/jugar`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: JSON.stringify({
            estadoAnterior
        })
      })
    .then(res => res.json())
    .then(data => data);
    return response;
}
exports.handleE = handleE;
exports.getGame = getGame;
exports.saveGame = saveGame;
exports.createGame = createGame;
