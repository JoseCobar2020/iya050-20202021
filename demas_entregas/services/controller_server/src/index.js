const util = require("./util.js");
const Koa = require("koa");
const cors = require("@koa/cors");
const koaBody = require("koa-body");
const Router = require("@koa/router");

const app = new Koa();
const router = new Router();

router.post("/game", async (ctx) => {
  // 1. post mutation to Stats server to store new game

  const nuevoJuego = await util.createGame();
  nuevoJuego.id = Math.floor((Math.random() * (99 - 0 + 1)) + 0);;//ctx.request.body.id;
  nuevoJuego.nickname = ctx.request.body.nickname;

  const juegoGuardado = await util.saveGame(nuevoJuego);
  juegoGuardado.juego = JSON.parse(juegoGuardado.juego);
  juegoGuardado.direccion = JSON.parse(juegoGuardado.direccion.replace(/'/g, '"'));
  juegoGuardado.ultimaPos = JSON.parse(juegoGuardado.ultimaPos.replace(/'/g, '"'));

  // 2. return state

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    state: juegoGuardado
    // ...
  });
});

router.get("/game/:id",  async (ctx) => {
  // 1. get current state from Stats server

    let response = await util.getGame(ctx.params.id);

  // 2. return state

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    status: response,
  });
});

router.post("/game/:id/event", async (ctx) => {
  // 1. get current state from Stats server

    let estadoActual = await util.getGame(ctx.params.id);

    estadoActual.juego = JSON.parse(estadoActual.juego)
    estadoActual.direccion = JSON.parse(estadoActual.direccion.replace(/'/g, '"'));
    estadoActual.ultimaPos = JSON.parse(estadoActual.ultimaPos.replace(/'/g, '"'));
    
    let movimiento = ctx.request.body.teclaPresionada !== undefined ? ctx.request.body.teclaPresionada : null; 
    estadoActual.movimiento = movimiento;

  // 2. get next state from Game server

    let siguienteEstado = await util.handleE(estadoActual);

  // 3. post mutation to Stats server to store next state

    const juegoGuardado = await util.saveGame(siguienteEstado);

  // 4. return state

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    esto: juegoGuardado
    // ...
  });
});

app.use(koaBody());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT || 5000);
