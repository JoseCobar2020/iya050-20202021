const KOA_url =  process.env.CONTROLLER_SERVER_URL || "http://localhost:5000"

import React, { useContext, useEffect, useState } from "react";
import AuthContext from "../AuthContext";
import 'regenerator-runtime/runtime'

const Play = () => {

const [loading, setLoading] = useState(false);

useEffect(async () => {

  setLoading(true);
  let juego = await nuevoJuego();

  setGame(juego);
  setLoading(false);

}, []);

const { user } = useContext(AuthContext);
const [ game, setGame] = useState(null);

  async function nuevoJuego(){

    let response = await fetch(`${KOA_url}/game`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
          nickname: user,
      })
    })
    .then(res => res.json())
    .then(data => data);
    
    return response;

  }

  let data = game === null ? <p>Cargando...</p> : <div>
    <div>
      {game.state.juego.map((linea, index) => {return <div key={`linea-${index}`}>{linea}</div>})}
    </div>
  </div>

  return( 
    <div>{data}</div>
  )
};

export default Play;
