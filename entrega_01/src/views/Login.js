import React, { useState } from 'react';
import{useHistory} from 'react-router-dom';

function Login() {

  const history = useHistory();
  const [uname, setUname] = useState("");
  const [password, setPassword] = useState("");

  const handleUnameChange = (event) =>{
      setUname(event.target.value)
  }
  const handlePasswordChange = (event) =>{
      setPassword(event.target.value)
  }

  const login = (uname, password) => {
    let url = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" + uname + "/sessions";
    let content = {
      method: "POST",
      body: JSON.stringify({
        password: password
      })
    }

    return fetch(url, content);
  
  }

  const submit = (event) => {
    event.preventDefault();
    let response;

    response = login(uname, password)
    .then(res => res.json())
    .then(res => {
        if(res.hasOwnProperty("sessionToken")){
            window.localStorage.setItem("token", res.sessionToken);
            history.push("/");
        }else{
            console.error("An error has occured. The login failed.");
        }
    })    
  }

  return (
    <form onSubmit={submit}>
      <div className="form-group">
        <label htmlFor="uname">Username address</label>
        <input id="uname" type="username" className="form-control" placeholder="Enter username" onChange={handleUnameChange}/>
      </div>
      <div className="form-group">
        <label htmlFor="password">Password</label>
        <input id="password" type="password" className="form-control" placeholder="Password" onChange={handlePasswordChange}/>
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  )
}
  
  export default Login;