import React, { useState } from 'react';
import{useHistory} from 'react-router-dom';

const Register = () => {

    const history = useHistory();
    const [uname, setUname] = useState("");
    const [name, setName] = useState("");
    const [password, setPassword] = useState("");

    const handleUnameChange = (event) =>{
        setUname(event.target.value)
    }
    const handleNameChange = (event) =>{
        setName(event.target.value)
    }
    const handlePasswordChange = (event) =>{
        setPassword(event.target.value)
    }

    const register = (uname, name, password) => {
        
        let url = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" + uname;
        let content = {
            method: "POST",
            body: JSON.stringify({
                firstName: name,
                password: password
            })
        }

        return fetch(url, content)
    }

    const submit = (event) => {
        event.preventDefault();
        let response;

        response = register(uname, name, password)
        .then(res => res.json())
        .then(res => {
            if(res.hasOwnProperty("username")){
                history.push("/Login")
            }else{
                console.error("An error has occured. The register failed.")
            }
        })    
    }

    return (
        <form onSubmit={submit}>
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <input id="uname" type="text" className="form-control" placeholder="Enter username" onChange={handleUnameChange}/>
        </div>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input id="name" type="text" className="form-control" placeholder="Enter name" onChange={handleNameChange}/>
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input id="password" type="password" className="form-control" placeholder="Password" onChange={handlePasswordChange}/>
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
    )
}

export default Register