import React, { useState, useEffect } from 'react';
import{useHistory} from 'react-router-dom';
import { start } from '../game/game';
import './game.css'

function Home() {

    const history = useHistory();
    const [userdata, setUserdata] = useState({});

    const uData = () => {
        let url = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/user"
        let content = {
            method: "GET",
            headers: {
                Authorization: window.localStorage.getItem("token")
            },
            mode: 'cors'
        }

        return fetch(url,content)
    }

    useEffect(() => {
        uData().then(res => res.json()).then(res => {
            console.log("im in");
            if (res.hasOwnProperty("username")) {
                setUserdata(res)
            } else {
                history.push("/Login");
            }
        }).catch(e => {
            console.error(e.message)
        })
    }, [])

    const logOut = () => {
        let url = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/sessions/" + window.localStorage.getItem("token")
        let content = {
            method: "DELETE",
            headers: {
                Authorization: window.localStorage.getItem("token")
            }
        }
        fetch(url,content).then(res=> {
            if(res.status == 204){
                window.localStorage.removeItem("token")
                history.push("/Login")
            }
        })


    }

    return (
        <div className="userData"> 
        <button onClick={start}>Play snake FOR FREE!</button>
        <h1>{userdata.username}</h1><button onClick={logOut}>Logout</button>
        <div>
        <h1 style={{color: '#000'}}>Snake</h1>
        <h3 style={{color: '#000'}}>Presentado por José Cóbar</h3>
        <p id="miPunt" style={{color: '#000'}}></p>
        <p id="hiPunt" style={{color: '#000'}}></p>
    </div>
    <div id="tablero"></div>
        </div>
    )
}
  
export default Home;