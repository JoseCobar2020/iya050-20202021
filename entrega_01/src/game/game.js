import { velocidad_snake, actualizar as actualizarSnake, dibujar as dibujarSnake, getCabeza, autocolision, start as sStart } from './snake.js'
import { actualizar as actualizarFood, dibujar as dibujarFood, puntos } from './rabbit.js'
import { limites } from './grid.js'

let lastRenderTime = 0
let gameOver = false
let tablero = null;

function main(currentTime) {
console.log('prueba1');
    if(gameOver){
        let mensaje = ""
        if(sessionStorage.getItem("highScore") > puntos){
            mensaje = "Perdiste, tu puntuación fue: "+puntos
        }else{
            mensaje = "¡Batiste el record! Tu puntuación fue: "+puntos
        }
        if(confirm(mensaje+'. Presiona OK para volver a jugar.')){
            window.location = '/'
        }

        return
    }

    window.requestAnimationFrame(main)
    const secondsSinceLastRender = (currentTime - lastRenderTime) / 1000

    if(secondsSinceLastRender < 1 / velocidad_snake) return

    lastRenderTime = currentTime
    actualizar()
    dibujar()
    document.getElementById("miPunt").innerHTML = "Tu puntuación: "+puntos
    document.getElementById("hiPunt").innerHTML = "Mayor puntuación: "+sessionStorage.getItem("highScore")
}


function actualizar() {

    actualizarSnake()
    actualizarFood()
    muerte()
}

function dibujar() {

    tablero.innerHTML = ''
    dibujarSnake(tablero)
    dibujarFood(tablero)
}

function muerte(){

    gameOver = limites(getCabeza()) || autocolision()
}

export const start = () => {
    tablero = document.getElementById('tablero')

    if(typeof(Storage) !== "undefined"){
        if(sessionStorage.getItem("highScore") == null){
            sessionStorage.setItem("highScore", 0)
        }
    }else{
        document.getElementById("result").innerHTML = "La puntuación no está disponible para este navegador."
    }
    window.requestAnimationFrame(main)
    sStart()
    main()
}