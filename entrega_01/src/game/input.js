let direccionSeleccionada = { x: 0, y: 0 }
let ultimaDireccionSeleccionada = { x: 0, y: 0 }



export const start = () => {
    window.addEventListener('keydown', e => {
        switch(e.key){
            case 'ArrowUp':
                if(ultimaDireccionSeleccionada.y !== 0) break
                direccionSeleccionada = { x: 0, y: -1 }
                break
            case 'ArrowDown':
                if(ultimaDireccionSeleccionada.y !== 0) break
                direccionSeleccionada = { x: 0, y: 1 }
                break
            case 'ArrowLeft':
                if(ultimaDireccionSeleccionada.x !== 0) break
                direccionSeleccionada = { x: -1, y: 0 } 
                break
            case 'ArrowRight':
                if(ultimaDireccionSeleccionada.x !== 0) break
                direccionSeleccionada = { x: 1, y: 0 }
                break
        }
    })
}

export function getDireccionSeleccionada(){
    ultimaDireccionSeleccionada = direccionSeleccionada
    return direccionSeleccionada
}