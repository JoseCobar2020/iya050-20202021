const tamaño_tablero = 21

export function posicionAleatoria(){

    return { x: Math.floor(Math.random() * tamaño_tablero) + 1, y: Math.floor(Math.random() * tamaño_tablero) + 1 }
}

export function limites(posicion){

    return (posicion.x < 1 || posicion.x > tamaño_tablero || posicion.y < 1 || posicion.y > tamaño_tablero)
}