import { getDireccionSeleccionada, start as istart } from "./input.js"
 
export const velocidad_snake = 15
const cuerpoSnake = [{ x: 11, y: 11 }]
let nuevosSegmentos = 0

export function actualizar() {
    
    añadirSegmentos()
    const direccionSeleccionada = getDireccionSeleccionada() 
    for(let i = cuerpoSnake.length -2; i >= 0; i--){
        cuerpoSnake[i + 1] = { ...cuerpoSnake[i]}
    }

    cuerpoSnake[0].x += direccionSeleccionada.x
    cuerpoSnake[0].y += direccionSeleccionada.y
}

export function dibujar(tablero) {

    cuerpoSnake.forEach(segment => {
        const snakeElement = document.createElement('div')
        snakeElement.style.gridRowStart = segment.y
        snakeElement.style.gridColumnStart = segment.x
        snakeElement.classList.add('snake')
        tablero.appendChild(snakeElement)
    });
}

export function crecimiento(cantidad){

    nuevosSegmentos += cantidad
}

export function colision(posicion, { ignorarCabeza = false } = {}){

    return cuerpoSnake.some((segment, index) => {
        if(ignorarCabeza && index === 0) return false
        return mismaPosicion(segment, posicion)
    })
}

export function getCabeza(){

    return cuerpoSnake[0]
}

export function autocolision(){

    return colision(cuerpoSnake[0], { ignorarCabeza: true })
}

function mismaPosicion(pos1, pos2){

    return pos1.x === pos2.x && pos1.y === pos2.y
}

function añadirSegmentos(){

    for(let i = 0; i < nuevosSegmentos; i++){

        cuerpoSnake.push({ ...cuerpoSnake[cuerpoSnake.length - 1] })
    }

    nuevosSegmentos = 0
}

export const start = () => {
    istart()
}