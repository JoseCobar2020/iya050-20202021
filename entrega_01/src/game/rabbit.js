import { colision, crecimiento } from './snake.js'
import { posicionAleatoria } from './grid.js'

let rabbit = getRabbitPosicionAleatoria()
const aumento = 5
export let puntos = 0

export function actualizar(){
    if(colision(rabbit)){
        crecimiento(aumento)
        rabbit = getRabbitPosicionAleatoria()
        
        puntos += 10
        if( puntos > sessionStorage.getItem("highScore")){
            sessionStorage.setItem("highScore", puntos)
        }
    }
}

export function dibujar(tablero) {

    const rabbitElement = document.createElement('div')
    rabbitElement.style.gridRowStart = rabbit.y
    rabbitElement.style.gridColumnStart = rabbit.x
    rabbitElement.classList.add('rabbit')
    tablero.appendChild(rabbitElement)
}

function getRabbitPosicionAleatoria(){

    let nuevaPosicion
    while(nuevaPosicion == null || colision(nuevaPosicion)){
        nuevaPosicion = posicionAleatoria()
    }

    return nuevaPosicion
}