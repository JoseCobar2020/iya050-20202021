const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    mode: 'development',//defines stage of our application. For development purpose it should be development and for deployment purpose we can use production.
    entry: './src/index.js',//defines entry point of our application which is ./src/index.js in our case.
    output: {//defines where our bundled file should be placed.
      filename: 'index.bundle.js',
      path: path.resolve(__dirname, 'dist'),
    },
    devtool: 'inline-source-map',//helps us to identify where exactly the error happened.
    module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader'
            }
          },
          {
            test: /\.s[ac]ss$/i,
            use: [
                // Creates `style` nodes from JS strings
                "style-loader",
                // Translates CSS into CommonJS
                "css-loader",
                // Compiles Sass to CSS
                "sass-loader",
            ],
        },
        {
            test: /\.css$/i,
            use: ["style-loader", "css-loader"],
        },
        {
            test: /\.(png|jpg)$/, loader: 'url-loader'
        }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),//This plugin first cleans the /dist folder and then put the bundled files inside that.
        new HtmlWebpackPlugin({//This plugin generates the html file which helps to serve the webpack bundles.
          template: './public/index.html'
        })
    ]
  }